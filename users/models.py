# users/models.py
from enum import IntEnum

from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import Group
from django.db import models


class CustomUser(AbstractUser):
    street = models.CharField(max_length=100, null=True, help_text="Freiwillige Angabe.", verbose_name="Straße", blank=True)
    zip_code = models.CharField(max_length=10, null=True, help_text="Freiwillige Angabe.", verbose_name="Postleitzahl", blank=True)
    city = models.CharField(max_length=100, null=True, help_text="Freiwillige Angabe.", verbose_name="Stadt", blank=True)
    country = models.CharField(max_length=100, null=True, help_text="Freiwillige Angabe.", verbose_name="Land", blank=True)
    phone = models.CharField(max_length=20, null=True, help_text="Freiwillige Angabe.", verbose_name="Telefonnummer", blank=True)
    email = models.EmailField(blank=False, verbose_name="E-Mail")

    def add_group(self, group):
        group.user_set.add(self)

    def __str__(self):
        return "CustomUser: " + self.first_name + " " + self.last_name + " (" + self.email + ")"


class MembershipType(IntEnum):
    NOT_CONFIRMED = 0
    MEMBER = 1
    ADMIN = 2


class UserGroupMembership(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    type = models.IntegerField(default=MembershipType.NOT_CONFIRMED)

    def __str__(self):
        return "User: " + self.user.get_full_name() + ", Group: " + self.group.name + " --> " \
               + str(MembershipType(self.type))
