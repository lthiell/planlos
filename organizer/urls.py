from django.conf.urls import url
from django.urls import path

from . import views

app_name = 'organizer'

urlpatterns = [
    path('', views.index, name='index'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('groups/join/', views.join_group, name="joinGroup"),
    path('groups/create/', views.create_group, name="create_group"),
    path('location-finder/', views.location_finder, name="location-finder"),
    path('location-finder/search', views.location_finder_query, name="location-finder/search"),
    path('groups/detail/', views.group_detail, name="groupDetail"),
    path('groups/member/reject', views.reject_group_member, name="reject_group_member"),
    path('groups/member/accept', views.accept_group_member, name="accept_group_member"),
    path('scheduler/add-event', views.add_scheduled_event, name="add_scheduled_event"),
    path('scheduler/delete-event', views.delete_event, name="delete_event"),
    path('event/', views.show_event_detail, name="show_event_detail"),
    path('event/delete-option', views.delete_event_option, name="delete_event_option"),
    path('event/finalize-option', views.finalize_event_option, name="finalize_event_option"),
    path('event/add-option', views.add_event_option, name="add_event_option"),
    path('event/stats', views.get_event_stat_row_html_fragment, name="get_event_stat_row_html_fragment"),
    path('event/change-participation', views.change_event_date_option_user_selection,
         name="change_event_date_option_user_selection"),
    path('groups/create-timeline-posting/', views.create_timeline_posting, name="createTimelinePosting"),
    path('impressum/', views.imprint, name="imprint"),
    path('datenschutz/', views.privacy, name="privacy"),
    path('account/edit', views.edit_account, name="edit_account"),
    url(r'^signup/$', views.signup, name='signup')
]
