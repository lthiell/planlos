from django.template.defaulttags import register

from organizer.services import event_service


@register.filter(name='get_item')
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter(name='edous_get_selection')
def edous_get_selection(event_date_option_user_selection):
    return event_service.get_event_date_option_user_selection_readable(event_date_option_user_selection.user_selection)


@register.filter(name='edous_get_id')
def edous_get_id(event_date_option_user_selection):
    if event_date_option_user_selection is None:
        return ""
    return event_date_option_user_selection.id


@register.filter(name='get_matching_error_messages')
def get_matching_error_messages(messages, extra_tag):
    matching_messages = []
    for message in messages:
        if message.extra_tags == extra_tag:
            matching_messages.append(message)
    return matching_messages
