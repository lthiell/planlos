from django.contrib.auth.models import Group

from organizer.services import geocoding_service


def get_groups_geographic_center(group: Group):
    users = group.user_set.all()
    coordinates = []
    for user in users:
        if user.street is not None and user.zip_code is not None:
            query_string = user.street + ", " + user.zip_code
            if user.city is not None:
                query_string = query_string + " " + user.city
            if user.country is not None:
                query_string = query_string + ", " + user.country
            geo_response = geocoding_service.get_lon_lat_from_query_string(query_string)
            if geo_response['geodata_present']:
                coordinates.append({'lat': geo_response['lat'], 'lon': geo_response['lon']})
    if coordinates.__len__() > 0:
        return get_geographic_center(coordinates)
    return None


def get_geographic_center(coordinates):
    result = {'lat': 0.0, 'lon': 0.0}
    for coor in coordinates:
        result['lat'] += float(coor['lat'])
        result['lon'] += float(coor['lon'])
    result['lat'] = result['lat'] / coordinates.__len__()
    result['lon'] = result['lon'] / coordinates.__len__()
    return result
