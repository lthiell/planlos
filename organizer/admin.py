from django.contrib import admin

from organizer.models import TimelineEntry, Event, EventDateOption, EventDateOptionUserSelection
from users.models import UserGroupMembership

admin.site.register(TimelineEntry)
admin.site.register(UserGroupMembership)
admin.site.register(Event)
admin.site.register(EventDateOption)
admin.site.register(EventDateOptionUserSelection)
