from typing import List

from django.contrib.auth.models import Group

from organizer.models import TimelineEntry
from users.models import CustomUser


def get_timeline_entries_for_groups(groups: Group) -> List[TimelineEntry]:
    return TimelineEntry.objects.order_by("-created_at").filter(group__in=groups)


def get_timeline_entries_for_group(group: Group) -> List[TimelineEntry]:
    return TimelineEntry.objects.order_by("-created_at").filter(group=group)


def add_timeline_entry_to_group(headline: str, text: str, group: Group, author: CustomUser):
    TimelineEntry.objects.create_timeline_entry(headline, text, group, author)