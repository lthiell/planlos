from typing import List

from django.contrib.auth.models import Group, User
from django.core.exceptions import ObjectDoesNotExist

from users.models import CustomUser, UserGroupMembership, MembershipType


def join_user_to_group(user, group):
    user.add_group(group)
    UserGroupMembership.objects.create(group=group, user=user, type=MembershipType.NOT_CONFIRMED)


def reject_user(user, group):
    user.add_group(group)
    group.user_set.remove(user)


def accept_user(user, group):
    membership_objs = UserGroupMembership.objects.filter(group=group, user=user)
    if membership_objs.count() > 0:
        membership_objs.update(type=MembershipType.MEMBER)
    else:
        UserGroupMembership.objects.create(group=group, user=user, type=MembershipType.MEMBER)


def get_confirmed_groups(user) -> List[Group]:
    groups = user.groups.all()
    confirmed_groups = []
    for g in groups:
        if is_member(user, g):
            confirmed_groups.append(g)
    return confirmed_groups


def get_all_unconfirmed_members(group):
    users = group.user_set.all()
    unconfirmed_members = []
    for u in users:
        if not is_member(u, group):
            unconfirmed_members.append(u)
    return unconfirmed_members


def get_all_confirmed_members(group):
    users = group.user_set.all()
    confirmed_members = []
    for u in users:
        if is_member(u, group):
            confirmed_members.append(u)
    return confirmed_members


def is_admin(user, group):
    try:
        return UserGroupMembership.objects.get(user=user, group=group).type == MembershipType.ADMIN
    except ObjectDoesNotExist:
        return False


def is_member(user, group):
    try:
        return UserGroupMembership.objects.get(user=user, group=group).type != MembershipType.NOT_CONFIRMED
    except ObjectDoesNotExist:
        return False


def get_all_groups(user: User) -> List[Group]:
    return user.groups.all()


def get_group_by_name(name: str) -> Group:
    return Group.objects.filter(name=name).first()


def get_group_by_id(id: str) -> Group:
    return Group.objects.filter(id=id).first()


def get_user_by_id(id: str) -> CustomUser:
    return CustomUser.objects.get(id=id)


def group_exists(name):
    return get_group_by_name(name=name) is not None


def create_group_with_admin_user(name, admin_user):
    group = Group.objects.create(name=name)
    admin_user.add_group(group)
    UserGroupMembership.objects.create(user=admin_user, group=group, type=MembershipType.ADMIN)
