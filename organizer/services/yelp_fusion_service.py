import requests
import logging

YELP_API_KEY = 'ZND_XvvWll19u_qHndY_G8VuHGXnnwJfKXwGoZu-XleUX2ko0vH_-uPM0zdIgy67mAsQE33-rJhTTbRVWMp_xsCaIKfYWPal_-voLU7fkwkZki8oFgv0btvywm2jXXYx'


def get_yelp_authentification_headers():
    return {"Authorization": "Bearer " + YELP_API_KEY}


def get_businesses_by_keyword(keyword, latitude, longitude, count):
    logger = logging.getLogger("yelp_fusion_service")
    request_params = {'term': keyword, "latitude": latitude, "longitude": longitude, 'locale': 'de_DE',
                      'sort_by': 'distance', 'limit': count}
    headers = get_yelp_authentification_headers()
    try:
        response = requests.get("https://api.yelp.com/v3/businesses/search", params=request_params, headers=headers,
                                timeout=10)
    except requests.exceptions.RequestException:
        logger.error("RequestException when requesting yelp fusion service with params: " + request_params.__str__())
        return ""
    if response.ok:
        return response.json()
    else:
        logger.error(
            "Yelp Fusion Api Request got status: " + str(response.status_code) + " with content: " + response.content)
        return ""
