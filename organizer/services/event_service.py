from organizer.models import Event, EventDateOption, EventDateOptionUserSelection


def create_event_with_options(host, group, title, location, description, choose_one_date, date_options):
    event = Event.objects.create(host=host, group=group, title=title, location=location, description=description,
                                 choose_one_date_option=choose_one_date)
    for do in date_options:
        EventDateOption.objects.create(event=event, date=do)
    return event.id


def get_events_for_group(group):
    return Event.objects.filter(group=group).order_by('-id')


def get_event_by_id(event_id):
    return Event.objects.filter(id=event_id).first()


def delete_event(event):
    return Event.objects.filter(id=event.id).delete()


def get_event_date_options(event):
    return EventDateOption.objects.filter(event=event)


def add_event_date_option(event, datetime):
    EventDateOption.objects.create(event=event, date=datetime)


def get_event_date_option_by_id(option_id):
    return EventDateOption.objects.filter(id=option_id).first()


def delete_event_date_option(option):
    final_option_of_event = get_final_option_of_event(option.event)
    if option == final_option_of_event:
        Event.objects.filter(id=option.event.id).update(final_option_id=None)
    return EventDateOption.objects.filter(id=option.id).delete()


def set_final_option_of_event(event, option):
    if option.event == event:
        Event.objects.filter(id=event.id).update(final_option_id=option.id)


def get_final_option_of_event(event):
    if not event.choose_one_date_option:
        return None
    if event.final_option_id is None:
        return None
    return EventDateOption.objects.filter(id=event.final_option_id).first()


def get_event_date_option_user_selections_by_user(option, users):
    user_option_dict = {}
    for user in users:
        user_option_dict[user.id] = EventDateOptionUserSelection.objects.filter(event_date_option=option,
                                                                                user=user).first()
    return user_option_dict


def get_event_date_option_user_selection_readable(selection_int):
    return event_date_option_types.get(selection_int)


def get_selection_dicts_by_option_id(event, users):
    options = EventDateOption.objects.filter(event=event)
    dict = {}
    for option in options:
        dict[option.id] = get_event_date_option_user_selections_by_user(option, users)
    return dict


def save_or_update_event_date_option_user_selection(event_date_option_id, user, selection_readable):
    if not event_date_option_types_by_readable.__contains__(selection_readable):
        return False
    event_date_option = EventDateOption.objects.filter(id=event_date_option_id).first()
    if event_date_option is None:
        return False
    existing_matching_selections = EventDateOptionUserSelection.objects.filter(event_date_option=event_date_option,
                                                                               user=user)
    user_selection = event_date_option_types_by_readable.get(selection_readable)
    if existing_matching_selections.first() is None:
        EventDateOptionUserSelection.objects.create(event_date_option=event_date_option, user=user,
                                                    user_selection=user_selection)
    else:
        existing_matching_selections.update(user_selection=user_selection)
    return True


def get_event_date_option_stat_dict(event):
    date_option_dict = {}
    date_options = EventDateOption.objects.filter(event=event)
    for option in date_options:
        date_option_dict[option.id] = {}
        date_option_dict[option.id]["NO"] = EventDateOptionUserSelection.objects.filter(
            event_date_option=option, user_selection=event_date_option_types_by_readable["NO"]).count()
        date_option_dict[option.id]["MAYBE"] = EventDateOptionUserSelection.objects.filter(
            event_date_option=option, user_selection=event_date_option_types_by_readable["MAYBE"]).count()
        date_option_dict[option.id]["YES"] = EventDateOptionUserSelection.objects.filter(
            event_date_option=option, user_selection=event_date_option_types_by_readable["YES"]).count()
    return date_option_dict


def reverse_dict(original_dict):
    new_dict = {}
    for k, v in original_dict.items():
        new_dict[v] = k
    return new_dict


event_date_option_types = {
    -1: "NO",
    0: "NOT_SELECTED",
    1: "MAYBE",
    2: "YES"
}

event_date_option_types_by_readable = reverse_dict(event_date_option_types)
