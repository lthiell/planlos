import requests
import logging


def get_lon_lat_from_query_string(query_string):
    logger = logging.getLogger("geocoding_service")
    request_params = {'q': query_string, 'format': 'json'}
    try:
        response = requests.get('https://nominatim.openstreetmap.org/search', params=request_params,
                                headers={'referer': 'plan-los.org'}, timeout=10)
    except requests.exceptions.RequestException:
        logger.error("RequestException when requesting geocoding service with params: " + request_params.__str__())
        return {'geodata_present': 0}
    if response.status_code != 200:
        logger.error("Geocoding service got other status code than 200: " + str(response))
        return {'geodata_present': 0}
    geodata = response.json()
    if len(geodata) > 0:
        return {'geodata_present': 1, 'lat': geodata[0].__getitem__('lat'), 'lon': geodata[0].__getitem__('lon'),
                'name': geodata[0].__getitem__('display_name')}
    else:
        return {'geodata_present': 0}
