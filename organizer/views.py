from datetime import datetime
from typing import List

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group, User
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.http import require_POST, require_GET

from organizer.components import geo_component
from organizer.models import TimelineEntry
from organizer.services import event_service, timeline_service, group_and_user_service, yelp_fusion_service
from users.forms import CustomUserCreationForm, CustomUserChangeForm
from users.models import CustomUser

COMMA = ","


def index(request):
    return render(request, 'organizer/index.html')


@login_required
@require_GET
def dashboard(request):
    groups = group_and_user_service.get_confirmed_groups(request.user)
    timelines_entries = timeline_service.get_timeline_entries_for_groups(groups)
    context = {"groups": groups, "timeline_entries": timelines_entries}
    return render(request, 'organizer/dashboard.html', context)


@login_required
@require_GET
def group_detail(request):
    try:
        group: Group = group_and_user_service.get_group_by_name(request.GET.__getitem__('g'))
    except MultiValueDictKeyError:
        return redirect('/dashboard/')
    user_groups: List[Group] = group_and_user_service.get_all_groups(request.user)
    if group not in user_groups:
        return redirect('/dashboard/')
    confirmed_user_groups: List[Group] = group_and_user_service.get_confirmed_groups(request.user)
    if group not in confirmed_user_groups:
        context = {"group": group}
        return render(request, 'organizer/group_detail_not_confirmed.html', context)
    timeline_entries: List[TimelineEntry] = timeline_service.get_timeline_entries_for_group(group)
    if group_and_user_service.is_admin(request.user, group):
        unconfirmed_members = group_and_user_service.get_all_unconfirmed_members(group)
    else:
        unconfirmed_members = []
    confirmed_members = group_and_user_service.get_all_confirmed_members(group)
    events = event_service.get_events_for_group(group)
    context = {"group": group, "timeline_entries": timeline_entries, "unconfirmed_members": unconfirmed_members,
               "confirmed_members": confirmed_members, "events": events}
    return render(request, 'organizer/group_detail.html', context)


@login_required
@require_POST
def join_group(request):
    if request.method == 'POST':
        if not request.POST.__contains__('group_name'):
            return redirect('/dashboard/')
        group: Group = group_and_user_service.get_group_by_name(request.POST.__getitem__('group_name'))
        if group is None:
            messages.error(request=request,
                           message='Es existiert keine Gruppe mit dem gesuchten Namen!',
                           extra_tags='join_group')
            return redirect('/dashboard/')
        current_user: User = request.user
        group_and_user_service.join_user_to_group(current_user, group)
        return redirect('/dashboard/')


@login_required
@require_POST
def create_group(request):
    if request.method == 'POST':
        if not request.POST.__contains__('group_name'):
            return redirect('/dashboard/')
        group_name = request.POST.__getitem__('group_name')
        if group_and_user_service.group_exists(group_name):
            messages.error(request=request,
                           message='Es existiert leider bereits eine Gruppe mit dem angegebenen Gruppennamen!',
                           extra_tags='create_group')
            return redirect('/dashboard')
        current_user: User = request.user
        group_and_user_service.create_group_with_admin_user(group_name, current_user)
        return redirect('/dashboard/')


def get_group_from_get_if_user_is_member(request):
    if request.GET.__contains__('groupId'):
        group = group_and_user_service.get_group_by_id(request.GET.__getitem__('groupId'))
        if group_and_user_service.is_member(request.user, group):
            return group
    return None


@login_required
@require_GET
def location_finder(request):
    group = get_group_from_get_if_user_is_member(request)
    if not group:
        return redirect('/dashboard/')
    center = geo_component.get_groups_geographic_center(group)
    if center is not None:
        group_center = {'caption': group.name, 'center': center}
        context = {'map_center': group_center, 'group': group}
    else:
        context = {}
    return render(request, 'organizer/location-finder.html', context)


@login_required
@require_GET
def location_finder_query(request):
    group = get_group_from_get_if_user_is_member(request)
    if not group:
        return redirect('/dashboard/')
    if not request.GET.__contains__('locationQuery'):
        return redirect('/map-playground')
    if request.GET.__contains__('currentMapCenter'):
        query_string_center = request.GET.__getitem__('currentMapCenter')
        if query_string_center == '':
            current_map_center = None
        else:
            cmc_list = query_string_center.replace('LatLng(', '').replace(')', '').split(',')
            if len(cmc_list) == 2:
                current_map_center = {'lat': cmc_list[0], 'lon': cmc_list[1]}
            else:
                current_map_center = None
    else:
        current_map_center = None
    location_query = request.GET.__getitem__('locationQuery')
    if current_map_center is not None:
        map_center = {'center': current_map_center}

    else:
        group_center = geo_component.get_groups_geographic_center(group)
        if group_center is not None:
            map_center = {'caption': group.name, 'center': group_center}
        else:
            map_center = None
    if map_center is not None:
        yelp_response = yelp_fusion_service.get_businesses_by_keyword(location_query, map_center["center"]["lat"],
                                                                      map_center["center"]["lon"], 40)
        context = {'yelp': yelp_response, 'map_center': map_center, 'group': group}
    else:
        context = {}
    return render(request, 'organizer/location-finder.html', context)


@login_required
@require_POST
def create_timeline_posting(request):
    group: Group = group_and_user_service.get_group_by_name(request.POST.__getitem__('group'))
    author: CustomUser = request.user
    text: str = request.POST.__getitem__('text')
    headline: str = request.POST.__getitem__('headline')
    if group and text != '' and headline != '':
        timeline_service.add_timeline_entry_to_group(headline, text, group, author)
        return redirect('/groups/detail?g=' + group.name)
    else:
        return dashboard(request)


@login_required
@require_POST
def accept_group_member(request):
    group: Group = group_and_user_service.get_group_by_name(request.POST.__getitem__('group_name'))
    members: List[CustomUser] = extract_members(request.POST.__getitem__('member_ids'))
    for member in members:
        group_and_user_service.accept_user(member, group)
    return HttpResponse(status=200);


@login_required
@require_POST
def reject_group_member(request):
    group: Group = group_and_user_service.get_group_by_name(request.POST.__getitem__('group_name'))
    members: List[CustomUser] = extract_members(request.POST.__getitem__('member_ids'))
    for member in members:
        group_and_user_service.reject_user(member, group)
    return HttpResponse(status=200)


@login_required
@require_POST
def add_scheduled_event(request):
    event_name = request.POST.__getitem__('eventName')
    event_location = request.POST.__getitem__('eventLocation')
    event_description = request.POST.__getitem__('eventDescription')
    event_choose_one_date = request.POST.__getitem__('choose_one_date') == "findDate"
    event_group_id = request.POST.__getitem__('groupId')
    event_group = Group.objects.get(id=event_group_id)
    event_dates = request.POST.getlist('date')
    event_times = request.POST.getlist('time')
    # should be avoided from frontend
    if len(event_times) != len(event_dates):
        return redirect('/dashboard/')
    event_date_times = []
    for i in range(len(event_dates)):
        event_date_times.append(event_dates[i] + ' ' + event_times[i])
    event_id = event_service.create_event_with_options(request.user, event_group, event_name, event_location,
                                                       event_description, event_choose_one_date, event_date_times)
    return redirect('/event?eventId=' + str(event_id))


@login_required
@require_POST
def delete_event(request):
    if not request.POST.__contains__('eventId'):
        return redirect('/dashboard')
    event_id = request.POST.__getitem__('eventId')
    event = event_service.get_event_by_id(event_id)
    if event is None:
        return redirect('/dashboard')
    redirect_url = '/groups/detail?g=' + event.group.name + '&tab=schedule'
    if not event.host == request.user:
        messages.error(request, "Der Termin konnte nicht gelöscht werden!")
    else:
        event_service.delete_event(event)
        messages.success(request, "Der Termin wurde erfolgreich gelöscht!")
    return redirect(redirect_url)


@login_required
def show_event_detail(request):
    if not request.GET.__contains__('eventId'):
        return redirect('/dashboard/')
    event_id = request.GET.__getitem__('eventId')
    event = event_service.get_event_by_id(event_id)
    users_groups = group_and_user_service.get_all_groups(request.user)
    if event.group not in users_groups:
        return redirect('/dashboard/')
    members = group_and_user_service.get_all_confirmed_members(event.group)
    selection_dict = event_service.get_selection_dicts_by_option_id(event, members)
    final_option = event_service.get_final_option_of_event(event)
    context = {'event': event, 'event_date_options': event_service.get_event_date_options(event),
               'members': members, 'selections': selection_dict, 'final_option': final_option}
    return render(request, 'organizer/event.html', context)


@login_required
@require_POST
def delete_event_option(request):
    if not (request.POST.__contains__('optionId')):
        return redirect('/dashboard')
    option_id = request.POST.__getitem__('optionId')
    option = event_service.get_event_date_option_by_id(option_id)
    if option is None:
        return redirect('/dashboard')
    if option.event.host == request.user:
        event_service.delete_event_date_option(option)
        messages.success(request, "Die Terminoption wurde erfolgreich gelöscht!")
    else:
        messages.error(request, "Die Terminoption konnte nicht gelöscht werden!")
    return redirect('/event?eventId=' + str(option.event.id))


@login_required
@require_POST
def finalize_event_option(request):
    if not (request.POST.__contains__('optionId')):
        return redirect('/dashboard')
    option_id = request.POST.__getitem__('optionId')
    option = event_service.get_event_date_option_by_id(option_id)
    if option is None:
        return redirect('/dashboard')
    if option.event.host == request.user:
        event_service.set_final_option_of_event(option.event, option)
        messages.success(request, "Die Terminumfrage wurde erfolgreich abgeschlossen!")
    else:
        messages.error(request, "Die Terminumfrage konnte nicht abgeschlossen werden!")
    return redirect('/event?eventId=' + str(option.event.id))


@login_required
@require_POST
def add_event_option(request):
    if not (request.POST.__contains__('eventId') and request.POST.__contains__('date') and request.POST.__contains__(
            'time')):
        return redirect('/dashboard')
    event_id = request.POST.__getitem__('eventId')
    date = request.POST.__getitem__('date')
    time = request.POST.__getitem__('time')
    event = event_service.get_event_by_id(event_id)
    if event is None:
        return redirect('/dashboard')
    event_service.add_event_date_option(event, date + ' ' + time)
    messages.success(request, "Die Terminoption wurde erfolgreich hinzugefügt!")
    return redirect('/event?eventId=' + str(event.id))


@login_required
@require_GET
def get_event_stat_row_html_fragment(request):
    if not request.GET.__contains__('eventId'):
        return redirect('/dashboard/')
    event_id = request.GET.__getitem__('eventId')
    event = event_service.get_event_by_id(event_id)
    stat_map = event_service.get_event_date_option_stat_dict(event)
    context = {'stat_map': stat_map, 'event_date_options': event_service.get_event_date_options(event)}
    return render(request, 'organizer/event_stat_row.html', context)


@login_required
@require_POST
def change_event_date_option_user_selection(request):
    date_option_id = request.POST.__getitem__('dateOptionId')
    selection_readable = request.POST.__getitem__('selection')
    success = event_service.save_or_update_event_date_option_user_selection(date_option_id, request.user,
                                                                            selection_readable)
    if success:
        return HttpResponse(status=200)
    return HttpResponse(status=400)


def signup(request):
    if request.user.is_authenticated:
        return redirect('/dashboard/')
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/dashboard/')
    else:
        form = CustomUserCreationForm()
    return render(request, 'organizer/signup.html', {'form': form})


def edit_account(request):
    if not request.user.is_authenticated:
        return redirect('organizer:signup')
    if request.method == 'POST':
        if not (request.POST.__getitem__('username') == request.user.username \
                and request.POST.__getitem__('first_name') == request.user.first_name \
                and request.POST.__getitem__('last_name') == request.user.last_name):
            form = CustomUserChangeForm(instance=request.user)
            set_readonly_fields(form)
            messages.error(request, "Deine Änderungen konnten nicht gespeichert werden!")
            return render(request, 'organizer/edit_account.html', {'form': form})
        form = CustomUserChangeForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, "Deine Änderungen wurden gespeichert!")
        else:
            messages.error(request, "Deine Änderungen konnten nicht gespeichert werden!")
    # Request Method not post
    else:
        form = CustomUserChangeForm(instance=request.user)
    set_readonly_fields(form)
    return render(request, 'organizer/edit_account.html', {'form': form})


def set_readonly_fields(account_edit_form):
    account_edit_form.fields['username'].widget.attrs['readonly'] = True
    account_edit_form.fields['first_name'].widget.attrs['readonly'] = True
    account_edit_form.fields['last_name'].widget.attrs['readonly'] = True


def imprint(request):
    return render(request, 'organizer/imprint.html')


def privacy(request):
    return render(request, 'organizer/privacy.html')


def extract_members(mids_string: str) -> List[CustomUser]:
    return list(map(lambda mid: group_and_user_service.get_user_by_id(mid),
                    map(lambda mid_str: int(mid_str),
                        mids_string.split(COMMA))))
