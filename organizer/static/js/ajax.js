"use strict";

export function post(body, url, onreadystatechange) {
    let csrf_token = document.querySelector("input[name='csrfmiddlewaretoken']").value;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.setRequestHeader("X-CSRFToken", csrf_token);
    xhr.send(body);
    xhr.onreadystatechange = onreadystatechange;
}

export function get(url, onreadystatechange) {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.send();
    xhr.onreadystatechange = onreadystatechange;
}