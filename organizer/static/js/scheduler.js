"use strict";

let btnAddEventDateTime;
let btnRemoveEventDateTime;
let formGroupDateTime;
let divDateTimes;

const init = function () {
    initVariables();
    addEventHandlers();
};

const initVariables = function () {
    btnAddEventDateTime = document.querySelector("#addEventDateTime");
    btnRemoveEventDateTime = document.querySelector("#removeEventDateTime");
    formGroupDateTime = document.querySelector("#dateTime");
    divDateTimes = document.querySelector("#dateTimes");
};

const addEventHandlers = function () {
    btnAddEventDateTime.addEventListener("click", () => copyDomElement(formGroupDateTime, divDateTimes));
    btnRemoveEventDateTime.addEventListener("click", () => removeLastDomElement("#dateTime"));
};

const copyDomElement = function (elem, destination) {
    let clone = elem.cloneNode(true);
    destination.appendChild(clone);
};

const removeLastDomElement = function (selector) {
    let allElems = document.querySelectorAll(selector);
    if (allElems.length > 1) {
        allElems[allElems.length - 1].remove();
    }
};

document.addEventListener("DOMContentLoaded", init);