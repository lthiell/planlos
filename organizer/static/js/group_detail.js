"use strict";

import {getUrlParameters} from "./urls_params.js";

const tabs = [
    {name: "timeline", tabPaneSelector: "#timeline", navLinkSelector: "#timelineTab"},
    {name: "members", tabPaneSelector: "#members", navLinkSelector: "#membersTab"},
    {name: "membershipRequests", tabPaneSelector: "#membershipRequests", navLinkSelector: "#membershipRequestsTab"},
    {name: "schedule", tabPaneSelector: "#schedule", navLinkSelector: "#scheduleTab"}
];

const hideAllTabs = function () {
    let tabs = document.querySelectorAll(".tab-pane");
    tabs.forEach(tab => tab.classList.remove("active", "show"));
    let navLinks = document.querySelectorAll(".nav-link");
    navLinks.forEach(navLink => navLink.classList.remove("active"));
};

const showTab = function (tabPaneSelector, navLinkSelector) {
    let tabPane = document.querySelector(tabPaneSelector);
    if (tabPane !== null) {
        hideAllTabs();
        tabPane.classList.add("active", "show");
        let navLink = document.querySelector(navLinkSelector);
        navLink.classList.add("active");
    }
};

const loadTabFromUrlParams = function () {
    let urlParameters = getUrlParameters();
    let tabParam = decodeURI(getUrlParameters()["tab"]);
    if (tabParam !== undefined && tabParam !== "undefined") {
        let matchingTabs = tabs.filter(t => t.name === tabParam);
        if (matchingTabs.length === 1) {
            showTab(matchingTabs[0].tabPaneSelector, matchingTabs[0].navLinkSelector);
        }

    }
};

const loadEventLocationFromUrlParams = function () {
    let urlParameters = getUrlParameters();
    let eventLocationParam = decodeURI(getUrlParameters()["eventLocation"]);
    if (eventLocationParam !== undefined && eventLocationParam !== "undefined") {
        let btnScheduleNewEvent = document.querySelector("#btnScheduleNewEvent");
        let inputEventLocation = document.querySelector("#eventLocation");
        if (btnScheduleNewEvent !== null && inputEventLocation !== null) {
            inputEventLocation.value = eventLocationParam;
            btnScheduleNewEvent.click();
            window.scrollTo(0, document.body.scrollHeight);
        }
    }
};

const onDOMContentLoaded = function () {
    loadEventLocationFromUrlParams();
    loadTabFromUrlParams();
};

document.addEventListener("DOMContentLoaded", onDOMContentLoaded);