"use strict";

import {post} from "./ajax.js";

const rejectUrl = "/groups/member/reject";
const acceptUrl = "/groups/member/accept";
let csrf_token;

const getSelection = function () {
    return Array.from(document.querySelectorAll(".unconfirmed_member"))
        .filter(elem => elem.querySelector("input").checked).map(elem => elem.dataset.memberId);
};

const onReadyStateChange = function () {
    location.reload();
};


const reject = function () {
    const userIds = getSelection();
    const groupName = document.querySelector("h1").textContent;
    const body = new FormData();
    body.append("group_name", groupName);
    body.append("member_ids", userIds);
    post(body, rejectUrl, onReadyStateChange);
};

const accept = function () {
    const userIds = getSelection();
    const groupName = document.querySelector("h1").textContent;
    const body = new FormData();
    body.append("group_name", groupName);
    body.append("member_ids", userIds);
    post(body, acceptUrl, onReadyStateChange);
};

const addEventListeners = function () {
    const rejectedBtn = document.querySelector("#rejectUnconfirmedMember");
    const acceptedBtn = document.querySelector("#acceptUnconfirmedMember");
    if (rejectedBtn && acceptedBtn) {
        rejectedBtn.addEventListener("click", reject);
        acceptedBtn.addEventListener("click", accept);
        csrf_token = document.querySelector("input[name='csrfmiddlewaretoken']").value;
    }
};


document.addEventListener("DOMContentLoaded", addEventListeners);

