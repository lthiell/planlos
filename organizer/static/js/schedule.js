"use strict";

let btnScheduleNewEvent;
let areaScheduleNewEvent;
let btnAddEventDateTime;
let btnRemoveEventDateTime;
let formGroupDateTime;
let divDateTimes;


const init = function () {
    initVariables();
    addEventHandlers();
};

const initVariables = function () {
    btnScheduleNewEvent = document.querySelector("#btnScheduleNewEvent");
    areaScheduleNewEvent = document.querySelector("#areaScheduleNewEvent");
    btnAddEventDateTime = document.querySelector("#addEventDateTime");
    btnRemoveEventDateTime = document.querySelector("#removeEventDateTime");
    formGroupDateTime = document.querySelector("#dateTime");
    divDateTimes = document.querySelector("#dateTimes");
};

const addEventHandlers = function () {
    btnScheduleNewEvent.addEventListener("click", toggleAreaScheduleNewEventVisibility);
    btnAddEventDateTime.addEventListener("click", () => copyDomElement(formGroupDateTime, divDateTimes));
    btnRemoveEventDateTime.addEventListener("click", () => removeLastDomElement("#dateTime"));
};

const copyDomElement = function (elem, destination) {
    let clone = elem.cloneNode(true);
    destination.appendChild(clone);
};

const removeLastDomElement = function (selector) {
    let allElems = document.querySelectorAll(selector);
    if (allElems.length > 1) {
        allElems[allElems.length - 1].remove();
    }
};

const toggleAreaScheduleNewEventVisibility = function () {
    if (areaScheduleNewEvent.style.display === "block") {
        areaScheduleNewEvent.style.display = "none";
    } else {
        areaScheduleNewEvent.style.display = "block"
    }
};

document.addEventListener("DOMContentLoaded", init);