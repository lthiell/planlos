"use strict";

import {get, post} from "./ajax.js";

const AJAX_POST_URL = "/event/change-participation";
const AJAX_GET_STATS_URL = "/event/stats";

const READY_STATE_COMPLETED = 4;
const HTTP_OK = 200;

const replaceStatAreaRowContent = function () {
    if (this.readyState === READY_STATE_COMPLETED && this.status === HTTP_OK) {
        document.getElementById("event-stat-row").innerHTML = this.responseText;
    }
};

const refreshStatArea = function () {
    let eventId = document.querySelector(".event-id").textContent;
    let url_with_params = AJAX_GET_STATS_URL + "?eventId=" + eventId;
    get(url_with_params, replaceStatAreaRowContent);

};

const postNewSelection = function (selection, dateOptionId) {
    const body = new FormData();
    body.append("selection", selection);
    body.append("dateOptionId", dateOptionId);
    post(body, AJAX_POST_URL, function () {
        if(this.readyState === READY_STATE_COMPLETED && this.status === HTTP_OK) {
            refreshStatArea();
        }
    });

};

const handleClickOnEventSelectionCell = function (event) {
    let icons;
    let tablecell;
    if (event.target.classList.contains("event-selection-icon")) {
        tablecell = event.target.parentElement;
    } else {
        tablecell = event.target;
    }
    icons = tablecell.querySelectorAll(".event-selection-icon");

    let i;
    for (i = 0; i < icons.length; i++) {
        if (icons.item(i).classList.contains("evt-selected")) {
            break;
        }
    }
    if (i < icons.length) {
        icons.item(i).classList.remove("evt-selected");
    }
    let selectedElement = icons.item((i + 1) % icons.length);
    selectedElement.classList.add("evt-selected");
    let dateOptionId = tablecell.querySelector(".event-date-option-id").textContent;
    postNewSelection(selectedElement.dataset.selection, dateOptionId);
};

const init = function () {
    addEventHandlers();
    refreshStatArea();
};

const addEventHandlers = function () {
    document.querySelectorAll("#tr-current-user .event-selection-tablecell").forEach(
        esi => esi.addEventListener("click", handleClickOnEventSelectionCell));
};

document.addEventListener("DOMContentLoaded", init);