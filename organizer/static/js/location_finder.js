"use strict";

import {getUrlParameters} from "./urls_params.js";

let locationQuery;
let locationTypeDropdown;


const parseLocationChoiceFromUrlsParams = function () {
    let locationQueryParam = decodeURI(getUrlParameters()["locationQuery"]);
    if (locationQueryParam !== undefined && locationQueryParam !== "undefined") {
        let categories = Array.from(document.querySelectorAll(".category")).map(elem => elem.textContent);
        if (categories.indexOf(locationQueryParam) > -1) {
            locationTypeDropdown.textContent = locationQueryParam;
        } else {
            locationTypeDropdown.textContent = document.querySelector("#category-own").textContent;
            locationQuery.style.display = "block";
        }
        locationQuery.value = locationQueryParam;
    }
};

const selectPredefinedLocation = function (evt) {
    locationQuery.value = evt.target.textContent;
    locationTypeDropdown.textContent = evt.target.textContent;
    locationQuery.style.display = "none";
};


const showOwnCategoryField = function (evt) {
    locationQuery.style.display = "block";
    locationQuery.value = "";
    locationTypeDropdown.textContent = evt.target.textContent;
};


const initMapPlayground = function (evt) {
    locationQuery = document.querySelector("#locationQuery");
    locationTypeDropdown = document.querySelector("#locationTypeDropdown");

    document.querySelectorAll(".category")
        .forEach(cat => {
            cat.addEventListener("click", selectPredefinedLocation);
        });
    document.querySelector("#category-own").addEventListener("click", showOwnCategoryField);
    parseLocationChoiceFromUrlsParams();
};

document.addEventListener("DOMContentLoaded", initMapPlayground);