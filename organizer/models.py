from datetime import timezone

from django.contrib.auth.models import Group
from django.db import models
from django.utils.timezone import now

from users.models import CustomUser, Group


class TimelineEntryManager(models.Manager):
    def create_timeline_entry(self, headline: str, text: str, group: Group, author: CustomUser):
        timeline_entry = self.create(headline=headline, text=text, created_at=now(), group=group,
                                     author=author)
        return timeline_entry


class TimelineEntry(models.Model):
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    headline = models.CharField(max_length=80)
    text = models.CharField(max_length=10000)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    objects = TimelineEntryManager()


class Event(models.Model):
    host = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=80)
    location = models.CharField(max_length=200)
    description = models.TextField(max_length=10000)
    choose_one_date_option = models.BooleanField()
    final_option_id = models.IntegerField(null=True)

    def __str__(self):
        return "Event: " + self.title + ", host: " + self.host.get_full_name() + ", location: " + self.location \
               + ", choose one date option: " + str(self.choose_one_date_option)


class EventDateOption(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    date = models.DateTimeField()

    def __str__(self):
        return "EventDateOption for Event: " + self.event.title + ", date: " + self.date.strftime('%Y-%m-%d %H:%M')


class EventDateOptionUserSelection(models.Model):
    event_date_option = models.ForeignKey(EventDateOption, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    user_selection = models.IntegerField()

    def __str__(self):
        return str(self.user_selection)
