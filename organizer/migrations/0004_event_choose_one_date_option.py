# Generated by Django 2.1.7 on 2020-01-15 08:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organizer', '0003_event_location'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='choose_one_date_option',
            field=models.BooleanField(default=True),
            preserve_default=False,
        ),
    ]
