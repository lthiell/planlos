# Generated by Django 2.1.7 on 2020-01-15 08:15

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('organizer', '0004_event_choose_one_date_option'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventdateoption',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='eventdateoptionuserselection',
            name='user_selection',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
