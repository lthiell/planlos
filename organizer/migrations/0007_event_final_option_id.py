# Generated by Django 2.1.7 on 2020-02-17 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organizer', '0006_event_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='final_option_id',
            field=models.IntegerField(null=True),
        ),
    ]
